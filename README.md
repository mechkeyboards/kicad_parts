# kicad_parts

Libraries and footprints are provided by Hasu, /u/techieee, mohitg11 and me.

## External links

* [Hasu's lib](https://github.com/tmk/kicad_lib_tmk)
* [Hasu's footprint](https://github.com/tmk/keyboard_parts.pretty)
* [/u/techieee's footprint](https://github.com/egladman/keebs.pretty)
* [mohitg11's footprints and libraries](https://github.com/mohitg11/TS65AVR)

## Kicad tips

* Usergrid (for MX placement): 0.09375 inches
* Usergrid (for Led placement): 0.1968504 inches
* Gap between Grid origin and edge cut: 
   - X: 0.53
   - Y: 0.50 mm
